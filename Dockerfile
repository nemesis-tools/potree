FROM node:10.17.0

ADD package.json /package.json

RUN npm install --save
RUN npm install -g gulp
RUN npm install -g rollup
