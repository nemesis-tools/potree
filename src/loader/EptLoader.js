/**
 * @author Connor Manning
 */

export class EptLoader {
	static async load(file, callback) {

		let xhr = Potree.XHRFactory.createXMLHttpRequest();
        xhr.open('GET', file, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    let json = JSON.parse(xhr.responseText)
                    let url = file.substr(0, file.lastIndexOf('ept.json'));
                    let geometry = new Potree.PointCloudEptGeometry(url, json);
                    let root = new Potree.PointCloudEptGeometryNode(geometry);

                    geometry.root = root;
                    geometry.root.load();

                    callback(geometry);
                } else {
                    console.log('Failed ' + url + ': ' + xhr.status);
                }
            }
        };

        xhr.send(null);
	}
};

