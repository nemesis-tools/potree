export const ZoneTypes = {
	EXCAVATION_TYPE_ZONE_TYPE: "excavation",
	BLAST_DATE_ZONE_TYPE: "blast_date",
	SECTOR_ZONE_TYPE: "sector",
	BOLT_PATTERN_ZONE_TYPE: "bolt_pattern",
	GEOLOGY_ZONE_TYPE: "geology",
};

export function getDefaultZoneVisibilityScheme() {
	return {
		"excavation": [0],
		"blast_date": [0],
		"sector": [0],
		"bolt_pattern": [0],
		"geology": [0]
	}
}

export function getDefaultClipZoneScheme() {
	return {
		"excavation": false,
		"blast_date": false,
		"sector": false,
		"bolt_pattern": false,
		"geology": false
	}
}
